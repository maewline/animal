/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.animal;

/**
 *
 * @author Arthit
 */
public class Crocodile extends Reptile {

    private String nickname;

    public Crocodile(String name) {
        super("Crocodile", 4);
        this.nickname = name;
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile name: " + nickname + " crawl.");
    }

    @Override
    public void eat() {
        System.out.println("Crocodile name: " + nickname + " eat.");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile name: " + nickname + " walk.");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile name: " + nickname + " can't speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile name: " + nickname + " sleep.");
    }

    @Override
    public String toString() {
        return "Animal nickname: "+nickname+super.toString();
    }
    
    

}
