/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.animal;

/**
 *
 * @author Arthit
 */
public class Crab extends Aquatic {

    private String nickname;

    public Crab(String name) {
        super("Crab", 8);
        this.nickname = name;
    }

    @Override
    public void swim() {
        System.out.println("Crab name: " + nickname + " swim.");
    }

    @Override
    public void eat() {
        System.out.println("Crab name: " + nickname + " eat.");
    }

    @Override
    public void walk() {
        System.out.println("Crab name: " + nickname + " can't walk.");
    }

    @Override
    public void speak() {
        System.out.println("Crab name: " + nickname + " can't speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Crab name: " + nickname + " sleep.");
    }

    public String toString() {
        return "Animal nickname: " + nickname + super.toString();
    }

}
