/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.animal;

/**
 *
 * @author Arthit
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human h1 = new Human("YodChai");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println(h1);
        Enter();

        Cat c1 = new Cat("MuMu");
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        c1.toString();
        System.out.println(c1);
        Enter();

        Dog d1 = new Dog("LauLa");
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        d1.toString();
        System.out.println(d1);
        Enter();

        Crocodile cd1 = new Crocodile("Ai-kae");
        cd1.eat();
        cd1.crawl();
        cd1.walk();
        cd1.speak();
        cd1.sleep();
        cd1.toString();
        System.out.println(cd1);
        Enter();

        Snake sn1 = new Snake("Jakim");
        sn1.eat();
        sn1.crawl();
        sn1.walk();
        sn1.speak();
        sn1.sleep();
        sn1.toString();
        System.out.println(sn1);
        Enter();

        Bat b1 = new Bat("LinHui");
        b1.eat();
        b1.fly();
        b1.walk();
        b1.speak();
        b1.sleep();
        b1.toString();
        System.out.println(b1);
        Enter();

        Fish f1 = new Fish("Buthong");
        f1.eat();
        f1.swim();
        f1.walk();
        f1.speak();
        f1.sleep();
        f1.toString();
        System.out.println(f1);
        Enter();

        Crab cb1 = new Crab("Puna");
        cb1.eat();
        cb1.swim();
        cb1.walk();
        cb1.speak();
        cb1.sleep();
        cb1.toString();
        System.out.println(cb1);
        Enter();

        Bird b = new Bird("NokNoiAuraiporn");
        b.eat();
        b.fly();
        b.walk();
        b.speak();
        b.sleep();
        b.toString();
        System.out.println(b1);
        Enter();

        System.out.println("Polymorphism Test...........................................................");
        Animal[] animals = {h1, c1, cd1, sn1, b1, f1, d1, cb1, b};
        for (int i = 0; i < animals.length; i++) {
            if (animals[i] instanceof Animal) {
                animals[i].eat();
                animals[i].walk();
                animals[i].speak();
                animals[i].sleep();

            }
            if (animals[i] instanceof LandAnimal) {
                System.out.println("Im in LandAnimal Class ");
                ((LandAnimal) (animals[i])).run();

            }
            if (animals[i] instanceof Reptile) {
                System.out.println("Im in Reptile Class ");
                ((Reptile) (animals[i])).crawl();

            }
            if (animals[i] instanceof Poultry) {
                System.out.println("Im in Poultry Class ");
                ((Poultry) (animals[i])).fly();

            }
            if (animals[i] instanceof Aquatic) {
                System.out.println("Im in Aquatic Class ");
                ((Aquatic) (animals[i])).swim();

            }
            Enter();
        }

        System.out.println("Polymorphism Test 2 ...........................................................");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].getName() + " Instanceof Animal " + (animals[i] instanceof Animal));
            System.out.println(animals[i].getName() + " Instanceof Aquatic " + (animals[i] instanceof Aquatic));
            System.out.println(animals[i].getName() + " Instanceof LandAnimal " + (animals[i] instanceof LandAnimal));
            System.out.println(animals[i].getName() + " Instanceof Poultry " + (animals[i] instanceof Poultry));
            System.out.println(animals[i].getName() + " Instanceof Reptile " + (animals[i] instanceof Reptile));
            System.out.println(animals[i].getName() + " Instanceof Object " + (animals[i] instanceof Object));
            Enter();
        }

    }

    private static void Enter() {
        System.out.println("");
    }
}
