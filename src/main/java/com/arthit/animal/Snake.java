/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arthit.animal;

/**
 *
 * @author Arthit
 */
public class Snake extends Reptile {

    private String nickname;

    public Snake(String name) {
        super("Snake", 0);
        this.nickname = name;
    }

    @Override
    public void crawl() {
        System.out.println("Snake name: " + nickname + " crawl.");
    }

    @Override
    public void eat() {
        System.out.println("Snake name: " + nickname + " eat.");
    }

    @Override
    public void walk() {
        System.out.println("Snake name: " + nickname + " can't walk.");
    }

    @Override
    public void speak() {
        System.out.println("Snake name: " + nickname + " can't speak.");
    }

    @Override
    public void sleep() {
        System.out.println("Snake name: " + nickname + " sleep.");
    }

    public String toString() {
        return "Animal nickname: " + nickname + super.toString();
    }

}
